package com.patsuyash.powerh.common;

import com.patsuyash.powerh.domain.Reading;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * This custom {@code ItemProcessor} simply writes the information of the
 * processed student to the log and returns the processed object.
 *
 * @author Petri Kainulainen
 */
public class LoggingReadingProcessor implements ItemProcessor<Reading, Reading> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingReadingProcessor.class);

    @Override
    public Reading process(Reading item) throws Exception {
        LOGGER.info("Processing student information: {}", item);
        return item;
    }
}
