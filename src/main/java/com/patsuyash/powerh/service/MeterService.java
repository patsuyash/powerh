package com.patsuyash.powerh.service;

import java.util.Date;

/**
 * Created by Suyash on 26-Jan-17.
 */

public interface MeterService {
    Integer getConsumtion(Date fromDate, Date toDate);
}
