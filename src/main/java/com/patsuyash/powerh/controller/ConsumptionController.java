package com.patsuyash.powerh.controller;

import java.util.Date;
import java.util.List;

import com.patsuyash.powerh.domain.MeterRepository;
import com.patsuyash.powerh.domain.PersonRepository;
import com.patsuyash.powerh.service.MeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.patsuyash.powerh.domain.Person;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;



@RestController
@RequestMapping("/consumption")
public class ConsumptionController {

    @Autowired
    private MeterService meterService;


    /**
     * /consumption/{fromDate}/{toDate}
     * @return
     */
    @RequestMapping(path="/", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Fetch all people")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Integer.class),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Server Error")})
    public Integer consumption(@RequestParam("fromDate") @DateTimeFormat(pattern="yyyy/MM/dd")  Date fromDate, @RequestParam("toDate") @DateTimeFormat(pattern="yyyy/MM/dd")  Date toDate) {

        return 0;//meterService.getConsumtion(fromDate, toDate);
    }



}
