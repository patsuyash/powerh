package com.patsuyash.powerh.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Suyash on 23-Jan-17.
 */
@Entity
@Data
//@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Meter {
    @Id
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reading> monthlyReadings = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Profile profile;

    public Meter(Long id, List<Reading> monthlyReadings, Profile profile) {
        this.id = id;
        this.monthlyReadings = monthlyReadings;
        this.profile = profile;
    }

    public Meter(long meterID) {
        this.id = id;
    }
}
