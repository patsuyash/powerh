package com.patsuyash.powerh.domain;

import lombok.*;

import javax.persistence.*;
import java.time.Month;

/**
 * Created by Suyash on 25-Jan-17.
 */
@Entity
@Data
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Reading {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private Month month;

    @Column(nullable = false)
    private Integer readingValue;

    @ManyToOne
    private Meter meter;

    public Reading(Month month, Integer readingValue, Meter meter) {
        this.month = month;
        this.readingValue = readingValue;
        this.meter = meter;
    }
}
