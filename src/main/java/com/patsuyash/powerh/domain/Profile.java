package com.patsuyash.powerh.domain;

import lombok.*;

import javax.persistence.*;
import java.time.Month;

/**
 * Created by Suyash on 24-Jan-17.
 */
@Entity
@Data
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Profile {
    @Id
    private Character id;
    @Column(nullable = false)
    private Month month;

    @Column(nullable = false)
    private Float fraction;

    public Profile(Character id, Month month, Float fraction) {
        this.id = id;
        this.month = month;
        this.fraction = fraction;
    }
}
