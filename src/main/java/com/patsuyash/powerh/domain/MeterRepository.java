package com.patsuyash.powerh.domain;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface MeterRepository extends PagingAndSortingRepository<Meter, Long> {

}
