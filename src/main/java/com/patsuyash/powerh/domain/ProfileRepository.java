package com.patsuyash.powerh.domain;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Suyash on 26-Jan-17.
 */

@RepositoryRestResource(collectionResourceRel = "profiles", path = "profiles")
public interface ProfileRepository extends PagingAndSortingRepository<Profile, Character> {
}
