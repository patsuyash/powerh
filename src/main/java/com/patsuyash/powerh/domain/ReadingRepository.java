package com.patsuyash.powerh.domain;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Suyash on 26-Jan-17.
 */
public interface ReadingRepository extends PagingAndSortingRepository<Reading, Long> {
}
