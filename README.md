
To view the generated Swagger UI documentation go to: [http://localhost:8080/swagger-ui.html]

Some useful functional concepts
Note: These concepts have been simplified for the sake of this exercise.
􀁸 Meter: is a device that is measuring the amount of gas or electricity being used within a house.
It's just a counter, so it will be a number that is increasing along time.
􀁸 Meter reading: It's the number that the Meter is showing at a specific date and time.
􀁸 Consumption: It's the difference, between two given date/times, of meter readings. Example: If
the meter reading on 2016/01/15 is 120 and the meter reading on 2016/02/15 is 150, then the
consumption between 2016/01/15 and 2016/02/15 is 30. Could be KWh, m3, but the unit of
measure is not relevant for this exercise.
􀁸 Profile: It's a collection of ratios [0..1], called Fractions, for every calendar month. It represents
how the consumption of a given year is distributed among months, so all the fractions for the 12
months must sum up 1. For example, for a house in the Netherlands it would be normal to have
higher ratios during winter than during summer, given that the energy consumed will be higher
because of heating.
Legacy